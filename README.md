# My Lab Project


## Aura Component


### SampleEmpApi

Accountの変更をキャプチャーするストリーミングAPIを使用したAura Componentのサンプル

はじめに以下のコマンドでPushTopicを作成してください

```sfdx
sfdx force:data:tree:import -f data/AccountChangeEvent-PushTopic.json 
```


### thirdparty_callout

外部のURLにAjaxでコールアウトするサンプル



### Default Account Create

標準の新規ボタンを上書きしたAuraでNameと関連するレコードIDを設定

ただし、モバイルでは動きません



### Simple Console Navigator

コンソールアプリケーションのバックグラウンドユーティリティとして登録することで

開いた時に強制的にナビゲーションを先頭の項目に変更します。

ただし、タイミングによってはワーキングタブの初期化と競合し、うまく動かない場合があります
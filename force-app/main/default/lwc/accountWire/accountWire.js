import { LightningElement, api, track } from 'lwc';

export default class AccountWire extends LightningElement {
    // Flexipage provides recordId and objectApiName
    @api recordId;
    @api objectApiName;
    @track time;

    handleLoad() {
        this.time = new Date();
    }
}
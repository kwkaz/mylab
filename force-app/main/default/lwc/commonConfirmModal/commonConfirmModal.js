import { LightningElement, api } from 'lwc';
/* eslint-disable no-console */

export default class CommonConfirmModal extends LightningElement {
    @api dialogtext;
    @api dialogtextArrayString;
    @api dialogheadertext
    @api cancellabel;
    @api executelabel;
    dialogtextlist;

    connectedCallback() {
        this.dialogtextlist = {};
        if ( this.dialogtextArrayString ){
            this.dialogtextlist = JSON.parse(this.dialogtextArrayString);
        }        
        console.log(this.dialogtextlist);
    }

    handleExecuteClicked(){
        const executeEvent = new CustomEvent('execute');
        this.dispatchEvent(executeEvent);
    }

    handleCancelClicked(){
        const cancelEvent = new CustomEvent('cancel');
        this.dispatchEvent(cancelEvent);
    }

}
import { LightningElement, api } from 'lwc';
import { subscribe, unsubscribe, onError, isEmpEnabled, setDebugFlag } from 'lightning/empApi';
/* eslint-disable no-console */

export default class BatchChecker extends LightningElement {
    @api jobId = '999999999999999';
    channelName = '/event/JobComplete__e';

    subscription = {};

    connectedCallback() {
        setDebugFlag({flag: true});
        this.handleSubscribe();
        
        
    }

    disconnectedCallback() {
        this.handleUnsubscribe();
    }


    // Handles subscribe button click
    handleSubscribe() {
        //console.log('subscribe:'+this.jobId);
        this.checkEmpApi();
        // Callback invoked whenever a new event message is received
        const messageCallback = function(response) {
            console.log('New message received : ', JSON.stringify(response));
            // Response contains the payload of the new message received
        };

        // Invoke subscribe method of empApi. Pass reference to messageCallback
        subscribe({ channel: this.channelName, replayId: -1, onMessageCallback: messageCallback} ).then(response => {
            // Response contains the subscription information on successful subscribe call
            console.log('Successfully subscribed to : ', JSON.stringify(response.channel));
            this.subscription = response;
        });
    }

    // Handles unsubscribe button click
    handleUnsubscribe() {

        // Invoke unsubscribe method of empApi
        unsubscribe(this.subscription, response => {
            console.log('unsubscribe() response: ', JSON.stringify(response));
            // Response is true for successful unsubscribe
        });
    }

    registerErrorListener() {
        // Invoke onError empApi method
        onError(error => {
            console.log('Received error from server: ', JSON.stringify(error));
            // Error contains the server-side error
        });
    }

    checkEmpApi(){
        isEmpEnabled().then(ret => {
            // Response contains the subscription information on successful subscribe call
            console.log('Successfully checkEmpApi: ', JSON.stringify(ret));
        }).catch(ret => {
            console.log('error occur: ' );
            console.log(ret);
        });
    }
}
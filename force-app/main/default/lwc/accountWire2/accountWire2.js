import { LightningElement, wire, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord } from 'lightning/uiRecordApi';

const FIELDS = ['Account.Name', 'Account.Phone'];

export default class AccountWire2 extends LightningElement {
    @api recordId;
    @track name;
    @track phone;

    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    accountRecord( {error, data} ){
        if (error){
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'error',
                    message: 'error',
                    variant: 'error',
                }),
            );

        } else if ( data ){
            this.name = data.fields.Name.value;
            this.phone = data.fields.Phone.value;
            
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'loaded',
                    message: 'load',
                    variant: 'info',
                }),
            );
            



        }

    }

}
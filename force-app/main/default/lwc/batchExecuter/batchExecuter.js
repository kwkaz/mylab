import { LightningElement,track,api,wire} from 'lwc';
//import getBatchRecordsWithJSON from '@salesforce/apex/BatchRecordController.getBatchRecordsWithJSON';
import getBatchRecords from '@salesforce/apex/BatchRecordController.getBatchRecords';
import start from '@salesforce/apex/BatchRecordController.start';
//import { CurrentPageReference } from 'lightning/navigation';
//import { NavigationMixin } from 'lightning/navigation';
/* eslint-disable no-console */

export default class BatchExecuter extends LightningElement {
    @track openConfirmModal;
    @track openResultModal;
    @api recordIds;
    @api returl;
    @track msg;
    @track recordString;
    @track jobId;

    @wire(getBatchRecords, { batchIdList: '$recordIds' } )
    //@wire(getBatchRecordsWithJSON, { batchIdListJSON: '["a00N000000NPrQbIAL", "a00N000000NPrQgIAL"]' } )
    //@wire(getBatchRecordsWithJSON, { batchIdListJSON: '$batchIdList' } )
    wiredBatchRecords( record ){
        console.log(record);
        console.log(JSON.stringify(record.data));
        if ( record.data ){
            this.msg = '以下のレコードに対してバッチを起動します';
            this.recordString = JSON.stringify(record.data);
            this.openConfirmModal = true;
        }
    }

    connectedCallback() {
        console.log(this.recordIds);
        console.log(JSON.stringify(this.recordIds));
        console.log(this.returl);
        // //this.batchIdList = this.recordIds;
        // this.batchIdList = '["a00N000000NPrQbIAL", "a00N000000NPrQgIAL"]';
        // // this.batchIdList = JSON.parse(this.recordIds);
        // getBatchRecordsWithJSON({batchIdListJSON: '["a00N000000NPrQbIAL", "a00N000000NPrQgIAL"]' }
        //     .then(result => {
        //         if ( result.data ){
        //             this.msg = '以下のレコードに対してバッチを起動します';
        //             this.recordString = JSON.stringify(result.data);
        //         }
        //     })
        //     .catch(error => {
        //         console.log(error);
        //     })
        // );
            

    }

    handleCancel(){
        this.goback();
        //window.open('https://ability-data-1453-dev-ed.lightning.force.com/lightning/o/BatchRecord__c/list?filterName=00BN00000037QPTMA2', '_top');
    }

    handleStart(){
        this.openConfirmModal = false;
        start({batchIdList: this.recordIds})
            .then(result => {
                console.log(result);
                this.jobId = result;
                this.msg = 'バッチを起動しました';
                this.openResultModal = true;
            })
            .catch(error => {
                console.log(error);
            });
    }

    goback(){
        console.log(decodeURI(this.returl));
        window.open(decodeURI(this.returl), '_top');
    }
}
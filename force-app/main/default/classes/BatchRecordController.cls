public with sharing class BatchRecordController {

    @AuraEnabled(cacheable=true)
    public static List<BatchRecord__c> getBatchRecords(List<Id> batchIdList ){
        return [ SELECT Id, Name FROM BatchRecord__c WHERE Id IN :batchIdList ];
    }

    @AuraEnabled(cacheable=true)
    public static List<BatchRecord__c> getBatchRecordsWithJSON(String batchIdListJSON ){
        List<String> batchIdList = (List<String>)System.JSON.deserialize(batchIdListJSON, List<String>.class);
        return [ SELECT Id, Name FROM BatchRecord__c WHERE Id IN :batchIdList ];
    }

    @AuraEnabled(cacheable=true)
    public static String start(List<Id> batchIdList){
        String jobId = Database.executeBatch(new BatchRecordBatchable(batchIdList));
        return jobId;
    }
}

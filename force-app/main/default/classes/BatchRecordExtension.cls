public with sharing class BatchRecordExtension {

    public String message{get; set;}
    private ApexPages.StandardSetController standardSetController;
    public List<Id> selectedIds{get; set;}
    public Boolean isConfirm{get; set;}
    public Boolean isComplete{get; set;}
    public String url{get; set;}
    public String selectedIdString{get; set;}

    public BatchRecordExtension(ApexPages.StandardSetController controller) {
        this.standardSetController = controller;
        this.isConfirm = true;
        this.isComplete = false;
        //this.url = ApexPages.currentPage().getUrl();
        this.url = ApexPages.currentPage().getParameters().get('vfRetURLInSFX' );
        System.debug(this.url);
        //this.url = EncodingUtil.urlEncode(this.url, 'UTF-8');
        //System.debug(this.url);
        init();
    }

    public PageReference init(){
        List<BatchRecord__c> selected = (List<BatchRecord__c>) standardSetController.getSelected();
        selectedIds = new List<Id>();
        message = 'Selected => ';
        for ( BatchRecord__c record : selected ){
            message += record.Id;
            selectedIds.add(record.Id);
        }
        selectedIdString = JSON.serialize(selectedIds);
        system.debug(selectedIdString);
        //selectedIdString = '[\'a00N000000NPrQbIAL\', \'a00N000000NPrQgIAL\']';

        return null;

    }

    public PageReference start(){
        String jobId = Database.executeBatch(new BatchRecordBatchable(selectedIds));
        this.isConfirm = false;
        this.isComplete = true;
        message = 'バッチを起動しました';

        return null;

    }
}

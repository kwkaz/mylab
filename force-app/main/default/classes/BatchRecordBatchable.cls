public with sharing class BatchRecordBatchable implements Database.Batchable<sObject>, Database.Stateful{

    private List<Id> ids;
    
    public BatchRecordBatchable(List<Id> ids) {
        this.ids = ids;
    }

    public Database.QueryLocator start( Database.BatchableContext bc){
        return Database.getQueryLocator( 
            [ SELECT Id, Counter__c FROM BatchRecord__c WHERE Id IN :ids ]);
    }

    public void execute( Database.BatchableContext bc, List<BatchRecord__c> scope ){
        for ( BatchRecord__c record : scope ){
            record.Counter__c = record.Counter__c + 1;
        }
        update scope;

    }

    public void finish( Database.BatchableContext bc){
        JobComplete__e jobEvent = new JobComplete__e(
           JobId__c = bc.getJobId(), 
           Status__c = 'Completed' );
        // Call method to publish events
        Database.SaveResult sr = EventBus.publish(jobEvent);
        // Inspect publishing result 
        if (sr.isSuccess()) {
            System.debug('Successfully published event.');
        } else {
            for(Database.Error err : sr.getErrors()) {
                System.debug('Error returned: ' +
                            err.getStatusCode() +
                            ' - ' +
                            err.getMessage());
            }
        }
    }
}

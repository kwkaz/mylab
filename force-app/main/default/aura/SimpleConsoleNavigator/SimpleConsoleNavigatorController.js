({
    doInit : function(component, event, helper) {
        let navigationItemAPI = component.find("navigationItemAPI");
        navigationItemAPI.getNavigationItems().then(function(response) {
            console.log("NavigationItems");
            console.log(response);
            // NavigationItemの先頭のアイテムを設定
            let itemName = response[0].developerName;

            navigationItemAPI.setSelectedNavigationItem({
                "developerName": itemName
            }).then(function(response) {
                navigationItemAPI.focusNavigationItem().then(function(response) {
                })
                .catch(function(error) {
                    console.log(error);
                });
            })
            .catch(function(error) {
                console.log(error);
            });
        })
        .catch(function(error) {
            console.log(error);
        });        

    }
})

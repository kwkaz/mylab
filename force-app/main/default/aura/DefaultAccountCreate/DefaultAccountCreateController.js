({
    doInit : function(component, event, helper) {
        var pageRef = component.get("v.pageReference");
        console.log('pageReference:'+JSON.stringify(pageRef));
        var state = pageRef.state; // state holds any query params
        console.log('state:'+state);
        var base64Context = state.inContextOfRef;
        console.log('base64Context:'+base64Context);

        //For some reason, the string starts with "1.", if somebody knows why,
        //this solution could be better generalized.
        if (base64Context.startsWith("1\.")) {
           base64Context = base64Context.substring(2);
        }
        var addressableContext = JSON.parse(window.atob(base64Context));
        console.log('addressableContext:'+JSON.stringify(addressableContext));
        component.set("v.recordId", addressableContext.attributes.recordId);
        var ev = $A.get("e.force:createRecord");
        ev.setParams({
            "entityApiName": "Account",
            "defaultFieldValues": {
                'Name' : '入力不要です',
            }
        });
        ev.fire();

    }
})

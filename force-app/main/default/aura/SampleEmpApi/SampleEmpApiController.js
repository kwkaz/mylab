({
    onButtonClicked : function(c, e, h) {

        const empApi = c.find('empApi');
        empApi.isEmpEnabled() // EMP Connector が利用可能か確認する
        .then($A.getCallback(function(isEmpEnabled) {

            if (isEmpEnabled) { // EMP Connector が利用可能

                // Streaming API との接続でエラーが発生した時のコールバックを登録
                empApi.onError($A.getCallback(function(error) {
                    console.error('EMP API error: ', error);
                }));

                // Channel に対し購読を開始
                let channel = '/topic/AccountChangeEvent'; // プッシュトピックの場合
                if ( c.get('v.recordId') != null ){
                    channel += '?Id=' + c.get('v.recordId');
                    console.log('channel=' + channel);
                }
                //const channel = '/data/AccountChangeEvent'; // 変更データキャプチャの場合
                const replayId = -1;
                empApi.subscribe(channel, replayId, $A.getCallback(function(event) {
                    const data = event.data;
                    console.log(JSON.stringify(data, null, 2));
                    c.set('v.result', JSON.stringify(data, null, 2));
                }));
                console.log('start');

            } else { // EMP Connector が利用不可
                console.error('EMP API error: ', 'EMP Connector is not supported');
            }
        }))        
    },
})